#include <proton/container.hpp>
#include <iostream>
#include "receiver1.h"
#include <thread>

using namespace std;

int main() {
    thread appThread([](){
        MyApp myApp;
        myApp.run();
    });

    string address("amqp:tcp://127.0.0.1:5672/mybroker");
    string topic("jms.topic.tasksecond"); 
    SimpleReceive receive(address, topic);
    
    thread receiveThread([&receive](){
        proton::container(receive).run();
    });

    appThread.join();
    receiveThread.join();

    return 0;
}
