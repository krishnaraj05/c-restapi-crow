import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DemoComponent } from './demo/demo.component';
import { LiveChartComponent } from './live-chart/live-chart.component';
import { ChartModule } from 'angular-highcharts';
import { D3Component } from './d3/d3.component';

@NgModule({
  declarations: [
    AppComponent,
    DemoComponent,
    LiveChartComponent,
    D3Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ChartModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
