import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as Highcharts from 'highcharts';
import { Observable, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-live-chart',
  templateUrl: './live-chart.component.html',
  styleUrls: ['./live-chart.component.css'],
})
export class LiveChartComponent {
  @ViewChild('chartcontainer') chartcontainer?: ElementRef;
  private chart: Highcharts.Chart | undefined;

  title = 'firstproject';
  data: any[] = [];
  numbers: number[] = [];
  constructor(private http: HttpClient) {}

  ngOnInit() {
    
    this.livechart();

    this.viveAPICall();
  }

  viveAPICall() {
    setInterval(() => {
      this.getData().subscribe((result: any) => {
      //  this.data=[];
        const innerArray = result[0];
        this.numbers=[];//print the latest data in the api
        for (const item of innerArray) {
          this.numbers.push(item.Number);
        }
     
        
        this.data = this.numbers.map((str) => +str);
        console.log("yes coming",this.data);
        this.chart?.series[0].setData(this.data);
      });
    }, 1000);
  }

  createChart() {
    const options: Highcharts.Options = {
      chart: {
        type: 'column',
      },
      title: {
        text: 'HEART-RATE',
      },
      series: [
        {
          type: 'line',
          name: 'Rate',
          data: this.data,
        },
      ],
    };
    // console.log(this.data);

    Highcharts.chart('chart-container', options);
  }

  livechart() {
    this.chart = Highcharts.chart('chartcontainer', {
      chart: {
        type: 'column',
      },
      title: {
        text: 'HEART-RATE',
      },
      series: [
        {
          type: 'spline',
          name: 'Rate',
          data: this.data,
        },
      ],
    });
  }

  getData(): Observable<any> {
    return  this.http.get('http://localhost:8080/hello')
    ;
  }
}
