import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { Observable, timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})

export class DemoComponent implements OnInit {
  title = 'firstproject';
  data: any[] = [];

  constructor(private http: HttpClient) {}

ngOnInit() {
  this.getData().subscribe((result: any) => {
    const numbers: number[] = [];
    const innerArray = result[0];
    for (const item of innerArray) {
      numbers.push(item.Number);
    }
    this.data = numbers.map(str => +str);
    this.createChart();
  });
}

createChart() {
  const options: Highcharts.Options = {
    chart: {
      type: 'column'
    },
    title: {
      text: 'HEART-RATE'
    },
    series: [{
      type: 'spline', 
      name: 'Rate',
      data: this.data 
    }]
  };
// console.log(this.data);

  Highcharts.chart('chart-container', options);
}

  getData(): Observable<any> {
    return timer(0, 10000) 
      .pipe(
        switchMap(() => this.http.get('http://localhost:8080/hello'))
      );
  }
}
