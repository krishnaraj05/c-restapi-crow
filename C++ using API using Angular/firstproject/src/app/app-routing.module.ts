import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoComponent } from './demo/demo.component';
import { LiveChartComponent } from './live-chart/live-chart.component';

const routes: Routes = [
  {path:'demo',component:DemoComponent},
  {path:'live-chart',component:LiveChartComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
