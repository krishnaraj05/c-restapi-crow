#include <proton/container.hpp>
#include <thread>
#include <iostream>
#include "sender1.h"

using namespace std;

int main() {
    string address("amqp:tcp://127.0.0.1:5672/mybroker");
    string topic("jms.topic.tasksecond");
    continuesender sender(address, topic);

    proton::container container(sender);

    thread container_thread([&]() { container.run(); });
    thread sender_thread([&]() { sender.send_message(); });
    
    sender_thread.join();
    container_thread.join();
    
    return 0;
}

