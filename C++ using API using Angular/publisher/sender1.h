#include <proton/messaging_handler.hpp>
#include <proton/container.hpp>
#include<mutex>

using namespace std;

class continuesender : public proton::messaging_handler {
private:
    string address;
    string topic;
    proton::sender sender;
    proton::connection connect;

public:
    continuesender(string &addr,string &top);
    void on_container_start(proton::container &c) override;
    void send_message();
};

